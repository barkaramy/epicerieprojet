class Article {
  constructor(articleName, articlePrice, articleQuantity) {
    this.articleName = articleName;
    this.articlePrice = articlePrice;
    this.articleQuantity = articleQuantity;
  }
}
