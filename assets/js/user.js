class User {
  constructor(Budget) {
    this.Budget = Budget;
  }

  checkBudgetCapacity(Cart) {
    if (Cart.total > this.Budget) {
      console.log("Désolé vous n'avez pas les fonds nécessaires");
    }
  }
}
