//import { Article } from './article';
//const Cart = require('./cart');
//import { Inventory } from './inventory';
//const User = require('./user');

let article1 = new Article('Salade', 20, 3);
let article2 = new Article('Viande', 50, 5);
let article3 = new Article('Pain', 10, 8);
let article4 = new Article('Olive', 5, 10);
let article5 = new Article('Pâtes', 4, 23);
let articles = [article1, article2, article3, article4, article5];
let inventory = new Inventory(articles);
let cart = new Cart();
const btnAdd = document.getElementById('btn-add');
const btnRemove = document.getElementById('btn-remove');

window.onload = initInventory();

function initInventory() {
  for (let i = 0; i < Object.keys(inventory.Articles).length; i++) {
    let tr = document.createElement('tr');
    tr.setAttribute('id', i);
    tr.setAttribute('onclick', 'selectInventoryArticle(' + i + ')');

    Object.values(inventory.Articles[i]).forEach(function(element) {
      let td = document.createElement('td');
      let data = document.createTextNode(element);
      td.appendChild(data);
      tr.appendChild(td);
    });
    let invent = document.querySelector('#Inventory tbody');
    invent.appendChild(tr).classList.add('article');
  }
}

function selectInventoryArticle(item) {
  let trblue = document.getElementById(item);
  let allArticles = document.getElementsByClassName('article');

  Array.prototype.forEach.call(allArticles, function(element) {
    element.classList.remove('bg-info');
  });
  trblue.classList.toggle('bg-info');
  //TODO verif si le parent a la classe cart ou inventory pour jouer avec le bouton add ou
  btnAdd.removeAttribute('disabled');
  btnRemove.removeAttribute('disabled');
}

btnAdd.addEventListener('click', handleBtnAdd);
btnRemove.addEventListener('click', handleBtnRemove);

function handleBtnAdd(e) {
  const elementSelected = document.querySelector('#Inventory .bg-info');

  let cartTable = document.querySelector('#Cart tbody');

  if (elementSelected != null) {
    cartTable.append(elementSelected);
    elementSelected.classList.remove('bg-info');
    btnAdd.setAttribute('disabled', true);
    btnRemove.setAttribute('disabled', true);
  }
}

function handleBtnRemove(e) {
  const elementSelected = document.querySelector('#Cart .bg-info');

  let inventoryTable = document.querySelector('#Inventory tbody');
  if (elementSelected != null) {
    inventoryTable.append(elementSelected);
    elementSelected.classList.remove('bg-info');
    btnRemove.setAttribute('disabled', true);
  }
}
